/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midterms;

import java.util.HashSet;

/**
 *
 * @author John Lloyd Lawas
 */
public class Midterms {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        HashSet<Student> set = new HashSet<Student>();
        SubjectClass java2 = new SubjectClass();

        set.add(new Student(16200063, "John Lloyd", "Lawas"));
        set.add(new Student(16200090, "Jefferson", "Manzano"));
        set.add(new Student(16200060, "Randel", "Pagligaran"));
        set.add(new Student(16200059, "Jan Bell", "Debalucos"));
        set.add(new Student(16200053, "Jomar", "Edaño"));
        set.add(new Student(16200021, "Oodah Mae", "Ermac"));
        set.add(new Student(16200055, "Roy", "Gallano"));
        set.add(new Student(16200040, "Xenna Mae", "Gomez"));
        set.add(new Student(16200041, "Shiell Mae", "Liwag"));
        set.add(new Student(16200042, "Jaya Althea", "Lazo"));
        set.add(new Student(16200043, "Anna May", "Tinaja"));
        set.add(new Student(16200044, "Riza Mae", "Barazon"));
        set.add(new Student(16200045, "Racquel", "Rosel"));
        set.add(new Student(16200046, "Madonna", "Real"));
        set.add(new Student(16200047, "Limelyn", "Lauglaug"));
        set.add(new Student(16200048, "Royena", "Belda"));
        set.add(new Student(16200048, "Arona", "Cartilla"));
        set.add(new Student(16200049, "Maricar", "Agunod"));
        set.add(new Student(16200028, "Wirly", "Tulod"));
        set.add(new Student(16200038, "Charlyn", "Jayme"));
        set.add(new Student(16200018, "Kristel", "Waskin"));

        java2.addStudent(set);

        java2.addStudent(new Student(16200065, "Eugene", "Cutamora"));
        
    }

}
