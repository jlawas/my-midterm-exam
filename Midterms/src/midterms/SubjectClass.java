/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midterms;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeSet;

/**
 *
 * @author John Lloyd Lawas
 */
public class SubjectClass {

    HashSet<Student> set;

    public SubjectClass() {
        set = new HashSet<Student>();
    }

    public void addStudent(Student s) {
        set.add(s);
        sortStudents();
    }

    public void addStudent(HashSet<Student> s) {
        set.addAll(s);
        sortStudents();
    }

    private void sortStudents() {

        TreeSet<String> sortedFamilyName = sortFamilyName();
        HashMap<Integer, Student> map = new HashMap<Integer, Student>();
        LinkedList<Student> list = new LinkedList<Student>();
        Iterator itr = sortedFamilyName.iterator();
        
        while (itr.hasNext()) {
            String surname = itr.next().toString();
            for (Student stud : set) {
                if (stud.getSurname().equals(surname)) {
                    list.add(stud);
                    map.put(stud.getId(), stud);
                }
            }
        }
        System.out.println("\n***************List of Student in Alphabetical order**********************");
        System.out.println("   ID      Lastname    Name");
        for (Student student : list) {
            System.out.println(student);
        }

    }

    private TreeSet<String> sortFamilyName() {
        TreeSet<String> sortStudent = new TreeSet<String>();
        for (Student temp : set) {
            sortStudent.add(temp.getSurname());
        }
        return sortStudent;
    }
}
